const makeChessboard = () => {
    let chessboard = []
    let bidakbelakang = ["B", "K", "M", "RT", "RJ", "M", "K", "B"];

    for (var i = 0; i < 8; i++) {
        chessboard.push([]);
    }

    for (var i = 0; i < 8; i++) {
        if (i == 0) {
            for (var j = 0; j < bidakbelakang.length; j++) {
                chessboard[i].push(bidakbelakang[j] + ' Black');
            }
        } else if (i == 1) {
            for (var j = chessboard[i].length; j < 8; j++) {
                chessboard[i].push("P Black");
            }
        } else if (i == 6) {
            for (var j = chessboard[i].length; j < 8; j++) {
                chessboard[i].push("P White");
            }
        } else if (i == 7) {
            for (var j = 0; j < bidakbelakang.length; j++) {
                chessboard[i].push(bidakbelakang[j] + ' White');
            }
        } else
            for (var j = chessboard[i].length; j < 8; j++) {
                chessboard[i].push("-");
            }
    }

    return chessboard
}

const printBoard = x => {
    console.log(x)
}

printBoard(makeChessboard())